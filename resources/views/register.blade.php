<html>
    <head>
        <title>form</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <h1>Buat Account Baru!</h1>
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="POST">
            @csrf
            <label for="namadepan">First Name:</label>
            <br><br>
            <input type="text" id="namadepan" name="depan">
            <br><br>
            <label for="namabelakang">Last Name:</label>
            <br><br>
            <input type="text" id="namabelakang" name="belakang">

            <p>Gender</p>
            <input type="radio" name="gender" id="male">
            <label for="male">Male</label>
            <br>
            <input type="radio" name="gender" id="female">
            <label for="female">Female</label>
            <br>
            <input type="radio" name="gender" id="OtherGender">
            <label for="OtherGender" >Other</label>

            <p>Nationality</p>
            <select>
                <option>Indonesian</option>
                <option>Singaporean</option>
                <option>Malaysian</option>
                <option>Australian</option>
            </select>

            <p>Language Spoken</p>
            <input type="checkbox" name="bahasa" id="bindo">
            <label for="bindo">Bahasa Indonesia</label>
            <br>
            <input type="checkbox" name="bahasa" id="english">
            <label for="english">English</label>
            <br>
            <input type="checkbox" name="bahasa" id="OtherLanguage">
            <label for="OtherLanguage" >Other</label>

            <p>Bio:</p>
            <textarea cols="100" rows="10"></textarea>
            <br><br>
            <input type="submit" value="Sign Up">   
        </form>
        
    </body>
</html>
