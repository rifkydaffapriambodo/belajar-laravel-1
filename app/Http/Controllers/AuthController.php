<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('register');
    }
    public function welcome(Request $r){
        $nama_lengkap = $r['depan']. ' '. $r['belakang'];
        return view('welcome', ['nama_lengkap' => $nama_lengkap]);
    }
}
